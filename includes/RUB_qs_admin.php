<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 27.04.2015
 * Time: 14:11
 */

/**
 * Create a new entry in the menu bar for all RUB plugins (if not already created). Then add entry for this plugin.
 */
function rub_qs_admin_menu(){

    // check whether global RUB admin menu page already exists, if not, create it!
    if ( empty ( $GLOBALS['admin_page_hooks']['RUB-reflection-plugins'] ) )
        add_menu_page(
            'RUB Reflection Plugins',
            'RUB Reflection Plugins',
            'manage_options',
            'RUB-reflection-plugins',
            'rub_qs_admin_menu_page_callback'
        );

    // afterwards, add sub entry for this plugin
    add_submenu_page(
        'RUB-reflection-plugins',
        'Question Scaffolding',
        'Question Scaffolding',
        'manage_options',
        'Question-Scaffolding',
        'rub_qs_admin_settings_form'
    );
}
add_action('admin_menu', 'rub_qs_admin_menu');

/**
 * Create settings section to hold the value for questions 1, 2 and 3
 */
function rub_qs_settings(){
    add_settings_section(
        'RUB-qs-settings',
        __('Question Scaffolding Settings','RUB_Questions'),
        'RUB_qs_settings_callback',
        'Question-Scaffolding'
    );

    add_settings_field(
        'RUB_qs_question_1',
        __('Question 1','RUB_Questions'),
        'RUB_qs_question_callback',
        'Question-Scaffolding',
        'RUB-qs-settings',
        array(
            'RUB_qs_question_1' // Should match Option ID
        )
    );

    add_settings_field(
        'RUB_qs_question_2',
        __('Question 2','RUB_Questions'),
        'RUB_qs_question_callback',
        'Question-Scaffolding',
        'RUB-qs-settings',
        array(
            'RUB_qs_question_2' // Should match Option ID
        )
    );

    add_settings_field(
        'RUB_qs_question_3',
        __('Question 3','RUB_Questions'),
        'RUB_qs_question_callback',
        'Question-Scaffolding',
        'RUB-qs-settings',
        array(
            'RUB_qs_question_3' // Should match Option ID
        )
    );

    register_setting('Question-Scaffolding', 'RUB_qs_question_1', 'esc_attr');
    register_setting('Question-Scaffolding', 'RUB_qs_question_2', 'esc_attr');
    register_setting('Question-Scaffolding', 'RUB_qs_question_3', 'esc_attr');
}
add_action('admin_init', 'rub_qs_settings');

/**
 * This method handles the display of the settings section in the admin menu
 */
function rub_qs_admin_settings_form() {
?>
    <div class="wrap">
        <form method="post" action="options.php">
            <?php
                //wp_nonce_field('update-options');
                settings_errors();
                settings_fields( 'Question-Scaffolding' );
                do_settings_sections( 'Question-Scaffolding' );
                submit_button();
            ?>
        </form>
    </div>
<?php
}

/**
 * Callback method to display input form for the questions
 * @param $args
 */
function rub_qs_question_callback($args) {
    // dont use translation here, let qTranslate-X do the work!

    $value = get_option($args[0]);
    echo '<input
        type="text"
        id="'.$args[0].'"
        class="'.$args[0].'"
        name="'.$args[0].'"
        size="50"
        value="'.$value.'"
        >';
    echo '<p class="description RUB_qs_question">'.$args[0].'</p>';
}

/**
 * This callback just shows the description for the settings section
 */
function rub_qs_settings_callback(){
    _e('The following questions can be selected for people to ask their peers in bbpress topics & replies', 'RUB_Questions');
}

/**
 * This callback can be used to display information on the overall admin page (not the subentry)
 */
function rub_qs_admin_menu_page_callback(){
    // Maybe show later all currently active plugins
}

function rub_qs_questions_updated($option, $old_value, $value){

    if($option == 'RUB_qs_question_1'){
        do_action('rub_qs_option_updated_question_1', $option, $old_value, $value);
    }

    if($option == 'RUB_qs_question_2'){
        do_action('rub_qs_option_updated_question_2', $option, $old_value, $value);
    }

    if($option == 'RUB_qs_question_3'){
        do_action('rub_qs_option_updated_question_3', $option, $old_value, $value);
    }
}
add_action('updated_option', 'rub_qs_questions_updated', 10, 3);
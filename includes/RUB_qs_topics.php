<?php
/**
 * Created by PhpStorm.
 * User: blunk
 * Date: 28.04.2015
 * Time: 10:59
 */

/**
 * Handles display of the questions (two questions and one input box). Added to the bbp_theme_before_topic_form_tags hook
 */
function rub_qs_action_bbp_theme_before_topic_form_tags(){

    echo "<div id='rub_qs_questions'>";
    echo '<label for="rub_qs_headline">' . __('Choose what you want to discuss about','RUB_Questions') . ' - </label>
        <label for="rub_qs_hint">' . __('These questions will be shown prominently to your readers','RUB_Questions') . '</label>
        <br>';

    $value = get_post_meta( bbp_get_topic_id(), 'rub_qs_question_4', true);
    $placeholder = __('Click here to type in your own question', 'RUB_Questions');
    echo "<div id='rub_qs_q4'>
        <input type='checkbox' id='rub_qs_question_4_cb' name='rub_qs_question_4_cb' value='1'>
        <input type='text' id='rub_qs_question_4' name='rub_qs_question_4' class='rub_qs_question_4' placeholder='$placeholder'><br>
    </div>";

    $value = get_post_meta( bbp_get_topic_id(), 'rub_qs_question_1', true);
    $question_1 = __(get_option('RUB_qs_question_1'), 'RUB_Questions');
    echo "<div id='rub_qs_q1'>
        <input type='checkbox' id='rub_qs_question_1' name='rub_qs_question_1' value='1'>
        <label for='rub_qs_question_1' class='rub_qs_question_label'>$question_1</label>
    </div>";

    $value = get_post_meta( bbp_get_topic_id(), 'rub_qs_question_2', true);
    $question_2 = __(get_option('RUB_qs_question_2'), 'RUB_Questions');
    echo "<div id='rub_qs_q2'>
        <input type='checkbox' id='rub_qs_question_2' name='rub_qs_question_2' value='1'>
        <label for='rub_qs_question_2' class='rub_qs_question_label'>$question_2</label>
    </div>";

    $value = get_post_meta( bbp_get_topic_id(), 'rub_qs_question_3', true);
    $question_3 = __(get_option('RUB_qs_question_3'), 'RUB_Questions');
    echo "<div id='rub_qs_q3'>
        <input type='checkbox' id='rub_qs_question_3' name='rub_qs_question_3' value='1'>
        <label for='rub_qs_question_3' class='rub_qs_question_label'>$question_3</label>
    </div>";
    echo "</div>";
}
add_action('bbp_theme_before_topic_form_tags', 'rub_qs_action_bbp_theme_before_topic_form_tags', 10, 0);

/**
 * Handles adding the questions to the post (topic) content upon adding & updating. Added to the new topic/update topic hooks
 * @param $topic_id
 * @return string
 */
function rub_qs_action_bbp_topic($topic_id){

    // cache questions
    $q1 =  get_option('RUB_qs_question_1');
    $q2 =  get_option('RUB_qs_question_2');
    $q3 =  get_option('RUB_qs_question_3');

    // save exact questions in post_meta
    // thus we can see later on which question was displayed, in case the question is changed throughout usage
    if (isset($_POST) && $_POST['rub_qs_question_1']!='')
        update_post_meta( $topic_id, 'rub_qs_question_1', $q1 );

    if (isset($_POST) && $_POST['rub_qs_question_2']!='')
        update_post_meta( $topic_id, 'rub_qs_question_2', $q2 );

    if (isset($_POST) && $_POST['rub_qs_question_3']!='')
        update_post_meta( $topic_id, 'rub_qs_question_3', $q3 );

    if (isset($_POST) && $_POST['rub_qs_question_4']!='' && $_POST['rub_qs_question_4_cb']!='')
        update_post_meta( $topic_id, 'rub_qs_question_4', $_POST['rub_qs_question_4'] );

    //get post
    $post = get_post($topic_id);
    //get content of post
    $content = $post->post_content;
    //http://wordpress.stackexchange.com/questions/9667/get-wordpress-post-content-by-post-id#comment110575_67255

    // Add questions if selected by user and not already existing in post
    if ($_POST['rub_qs_question_1']=='1' && (strpos($content, $q1) == false)){
        $content = $content . "\n" . $q1;
    }

    if ($_POST['rub_qs_question_2']=='1' && (strpos($content, $q2) == false)){
        $content = $content . "\n" . $q2;
    }

    if ($_POST['rub_qs_question_3']=='1' && (strpos($content, $q3) == false)){
        $content = $content . "\n" . $q2;
    }

    if ($_POST['rub_qs_question_4']!='' && $_POST['rub_qs_question_4_cb']!='' ){
        $content = $content . "\n" . $_POST['rub_qs_question_4'];
    }

    //write stuff back
    $post->post_content=$content;
    wp_update_post($post);

	$user_id = get_current_user_id();
	if($_POST['rub_qs_question_1']!='') { do_action('rub_q_user_selected_q1', $user_id, $topic_id, $question = $q1 ); }
	if($_POST['rub_qs_question_2']!='') { do_action('rub_q_user_selected_q2', $user_id, $topic_id, $question = $q2 ); }
	if($_POST['rub_qs_question_3']!='') { do_action('rub_q_user_selected_q3', $user_id, $topic_id, $question = $q3 ); }
	if($_POST['rub_qs_question_4']!='' && $_POST['rub_qs_question_4_cb']!='' )
	    { do_action('rub_q_user_selected_an_own_question', $user_id, $topic_id, $question = $_POST['rub_qs_question_4'] ); }

    return $content;
}
add_action('bbp_new_topic', 'rub_qs_action_bbp_topic', 10, 1);
add_action('bbp_edit_topic', 'rub_qs_action_bbp_topic', 10, 1);
<?php

/*
Plugin Name: RUB Question Scaffolding
Plugin URI: http://not-available.yet
Description: The plugin allows users to select predefined questions & write own questions while writing topics and replies in bbPress. Those questions try to help in keeping discussions alive and prompt readers to answer the questions. Questions can be set through an administrator interface.
Version: 0.1
Author: blunk
Author URI: http://not-available.yet
License: All rights reserved
*/
//TODO: Don't display question checkbox if question is empty
//TODO: option that users can select only one question
//to do: Add feature to set number of questions to display

// Adding questions to replies is currently on hold!
// to do: save questions when updating a reply
// to do: Extend to have different questions for topics and replies!

include 'includes/RUB_qs_admin.php';
include 'includes/RUB_qs_topics.php';

// Manual to integrate this plugin with qTranslate-X for backend translation
// add "rub_qs_question_1 rub_qs_question_2 rub_qs_question_3" without "" into Settings/Languages/'Custom fields' section / 'id' textbox <<<< those are responsible to identify the question input in the frontend
// add "RUB_qs_question_1 RUB_qs_question_2 RUB_qs_question_3" without "" into Settings/Languages/'Custom fields' section / 'classes' textbox <<<< those are responsible for accessing the backend input fields in the admin menu
// add "admin.php?page=Question-Scaffolding" into Settings/Languages/'Custom Admin Pages' textbox <<<< this is responsible to display the language buttons (like in new posts)

/*
 * create settings upon first activation, set english question and let admin translate manually desired questions
 */
function rub_qs_activate_plugin(){
    update_option('RUB_qs_question_1','What have you learned from this discussion so far?');
    update_option('RUB_qs_question_2','What would you personally suggest from your experience? Why?');
    update_option('RUB_qs_question_3','Have you ever had a similar experience? What happened?');
}
register_activation_hook(__FILE__, 'rub_qs_activate_plugin');

/*
 * Load css and js files
 */
function rub_qs_load_css_js(){
    wp_register_style('rub_qs_style', plugins_url('/css/rub_qs_style.css',__FILE__ ));
    wp_enqueue_style('rub_qs_style');

    wp_register_script('rub_qs_textbox', plugin_dir_url(__FILE__) . 'js/rub_qs_textbox.js', array('bootstrap','jquery'));
    wp_enqueue_script('rub_qs_textbox');
}
add_action('wp_enqueue_scripts', 'rub_qs_load_css_js');

/*
 * load language files
 */
function rub_qs_load_textdomain() {
    load_plugin_textdomain( 'RUB_Questions', false, basename(dirname(__FILE__)) . '/languages' );
}
add_action('plugins_loaded', 'rub_qs_load_textdomain');

/*
 * Hooks
 * do_action('rub_q_user_selected_q1', $user_id, $topic_id, $question = $q1 );          => Fires when question 1 was selected
 * do_action('rub_q_user_selected_q2', $user_id, $topic_id, $question = $q2 );          => Fires when question 2 was selected
 * do_action('rub_q_user_selected_q3', $user_id, $topic_id, $question = $q3 );          => Fires when question 3 was selected
 * do_action('rub_q_user_selected_an_own_question', $user_id, $topic_id, $question );   => Fires when user phrased his/her own question
 *
 * do_action('rub_qs_option_updated_question_1', $option, $old_value, $value);          => Fires when option for question 1 was updated
 * do_action('rub_qs_option_updated_question_2', $option, $old_value, $value);          => Fires when option for question 2 was updated
 * do_action('rub_qs_option_updated_question_3', $option, $old_value, $value);          => Fires when option for question 3 was updated
 */
